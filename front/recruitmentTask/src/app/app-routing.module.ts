import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductsListComponent } from '../app/domain/products/products-list/products-list.component';
import { AddEditProductComponent } from '@domain/products/add-edit-product/add-edit-product.component';

const routes: Routes = [
  {path: 'products', component: ProductsListComponent, pathMatch:'full'},
  {path: 'products/new', component: AddEditProductComponent, pathMatch:'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
