import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject, first, takeUntil } from 'rxjs';
import { Product } from 'src/app/core/models/products/product';
import { ProductsService } from 'src/app/core/services/products.service';
import { greaterThanZero, inputWhiteSpaceValidator } from 'src/app/core/validators';

@Component({
  selector: 'app-add-edit-product',
  templateUrl: './add-edit-product.component.html',
  styleUrls: ['./add-edit-product.component.css']
})
export class AddEditProductComponent implements OnInit, OnDestroy {

  productForm: FormGroup;
  id: string;
  loading = false;
  submitted = false;

  private unsubscribe$ = new Subject<void>();

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private productsService: ProductsService,
  ) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];

    this.productForm = this.formBuilder.group({
      name: new FormControl('', [inputWhiteSpaceValidator()]),
      barcode: new FormControl('', [inputWhiteSpaceValidator()]),
      price: new FormControl('', [greaterThanZero]),
    })
  }

  onSubmit() {
    this.submitted = true;

    if (this.productForm.invalid) {
      return;
    }
    this.createProduct();
  }

  private createProduct() {
    const query: Product = {
      name: this.productForm.get('name')?.value,
      barcode: this.productForm.get('barcode')?.value,
      price: this.productForm.get('price')?.value,
    }

    this.productsService.createProduct(query)
      .pipe(
        first(),
        takeUntil(this.unsubscribe$)
      )
      .subscribe(() => {
        this.router.navigate(['products']);
      });
  }

  onCancel(): void {
    this.router.navigate(['products']);
  }

  get productFormControl() {
    return this.productForm.controls;
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
