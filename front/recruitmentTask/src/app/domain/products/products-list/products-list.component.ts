import { Component, OnDestroy, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Subject, first, takeUntil } from 'rxjs';
import { Product } from 'src/app/core/models/products/product';
import { ProductsService } from 'src/app/core/services/products.service';
import { MatPaginator } from '@angular/material/paginator';

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.css']
})
export class ProductsListComponent implements OnDestroy {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  
  products: Product[] = [];
  displayedColumns: string[] = ['name', 'barcode', 'price'];
  dataSource: MatTableDataSource<Product>;

  private unsubscribe$ = new Subject<void>();

  constructor(
    private readonly productsServices: ProductsService,
    private readonly router: Router,
  ){}

  public getAllProducts(){
    this.productsServices.getAllproducts()
    .pipe(
      first(),
      takeUntil(this.unsubscribe$)
    )
    .subscribe(data => {
      this.dataSource = new MatTableDataSource<Product>(data);
      this.dataSource.paginator = this.paginator;
    });
  }

  public navigation(){
    this.router.navigate(['products/new']);
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
