import { NgModule } from '@angular/core';
import { MatTableModule } from '@angular/material/table';
import { MatCommonModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatButtonModule } from '@angular/material/button';
import { MatPaginatorModule } from '@angular/material/paginator';

const scharedModule = [ 
  MatAutocompleteModule,
  MatTableModule,
  MatCommonModule,
  MatFormFieldModule,
  MatInputModule,
  MatButtonModule,
  MatPaginatorModule,
]

@NgModule({
  exports: [
    scharedModule
  ],
  imports: [
    scharedModule
  ]
})
export class AngularMaterialModule { }
