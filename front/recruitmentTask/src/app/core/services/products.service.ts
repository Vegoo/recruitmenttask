import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Product } from '../models/products/product';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  public baseUrl = environment.apiUrl
  constructor(
    private http: HttpClient
  ) { }

  public getAllproducts(): Observable<Product[]> {
    return this.http
      .get<Product[]>(this.baseUrl + 'Product')
      .pipe((response: any) => response);
  }

  public createProduct(data: Product): Observable<null> {
    return this.http
      .post(this.baseUrl + 'Product', data)
      .pipe((response: any) => response);
  }
}
