export interface Product{
    name: string,
    barcode: string,
    price: number
}