import { AbstractControl, ValidatorFn } from "@angular/forms";

export function inputWhiteSpaceValidator() : ValidatorFn {
   return (control: AbstractControl): { [key: string]: any } | null => {
      if (control.value && control.value.trim() === '') {
         return { 'whitespace': true };
      }
      return null;
   };
}

export function greaterThanZero() : ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
       if (control.value && +control.value <= 0) {
          return { 'value': true };
       }
       return null;
    };
}