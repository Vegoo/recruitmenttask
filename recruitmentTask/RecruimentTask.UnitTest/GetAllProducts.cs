﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using RecruitmentTask.DAL;
using RecruitmentTask.DAL.Entities;
using RecruitmentTask.Services.Command;
using RecruitmentTask.Services.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecruimentTask.UnitTest
{
    public class GetAllProducts
    {
        [Test]
        public async Task GetAllProducts_ReturnListOfProducts()
        {
            var temp = new List<Product>()
            {
                new Product()
                {
                    Name = "jeden",
                    Barcode = "jeden",
                    Price = 11
                },
                new Product()
                {
                    Name = "dwa",
                    Barcode = "dwa",
                    Price = 22
                },
                new Product()
                {
                    Name = "trzy",
                    Barcode = "trzy",
                    Price = 33
                }
            };

            CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();

            var context = ProductTestHelper.GetDatabaseContext();

            context.Products.RemoveRange(context.Products);

            context.AddRange(temp);
            context.SaveChanges();

            var handler = new GetAllProductsQueryHandler(context);

            var result = await handler.Handle(new GetAllProductsQuery(), cancellationTokenSource.Token);

            Assert.That(result.Count(), Is.EqualTo(3));
        }
    }
}
