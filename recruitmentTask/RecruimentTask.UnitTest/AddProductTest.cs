﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Moq;
using NUnit.Framework;
using RecruitmentTask.DAL;
using RecruitmentTask.DAL.Entities;
using RecruitmentTask.Services.Command;
using RecruitmentTask.Services.Exceptions;
using RecruitmentTask.Commons;
using System.Security.Claims;

namespace RecruimentTask.UnitTest
{
    public class AddProductTest
    {
        [Test]
        public async Task AddProduct_WithCorrectData_ReturnUnit()
        {
            var product = new AddProductCommand()
            {
                Name = "Test",
                Barcode = "Test",
                Price = 0.01
            };

            CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();

            var context = ProductTestHelper.GetDatabaseContext();

            context.Products.RemoveRange(context.Products);
            context.SaveChanges();

            var handler = new AddProductCommandHandler(context);

            var result = await handler.Handle(product, cancellationTokenSource.Token);

            Assert.That(result, Is.EqualTo(Unit.Value));
            Assert.That(context.Products.Count(), Is.EqualTo(1));
        }

        [Test]
        public async Task AddProduct_WithDuplicateData_ReturnBadRequest()
        {
            var product1 = new Product()
            {
                Name = "Test2",
                Barcode = "Test2",
                Price = 0
            };

            var product2 = new AddProductCommand()
            {
                Name = "Test2",
                Barcode = "Test2",
                Price = 0
            };

            CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();

            var context = ProductTestHelper.GetDatabaseContext();

            context.Products.RemoveRange(context.Products);

            context.Products.Add(product1);
            context.SaveChanges();

            var handler = new AddProductCommandHandler(context);

            Assert.ThrowsAsync(Is.TypeOf<BadRequestException>()
                .And.Message.EqualTo(ErrorMessages.BarcodeInUsing), async delegate { await handler.Handle(product2, cancellationTokenSource.Token); });
        }

        [Test]
        public async Task AddProduct_WithPriceLowerThanZero_ReturnBadRequest()
        {
            var product = new AddProductCommand()
            {
                Name = "Test2",
                Barcode = "Test2",
                Price = -12
            };

            CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();

            var context = ProductTestHelper.GetDatabaseContext();

            context.Products.RemoveRange(context.Products);
            context.SaveChanges();

            var handler = new AddProductCommandHandler(context);

            Assert.ThrowsAsync(Is.TypeOf<BadRequestException>()
                .And.Message.EqualTo(ErrorMessages.PriceLowerThanZero), async delegate { await handler.Handle(product, cancellationTokenSource.Token); });
        }
    }
}
