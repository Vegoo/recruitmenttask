﻿namespace RecruitmentTask.Commons
{
    public static class ErrorMessages
    {
        public static string BarcodeInUsing = "Barcode is already in using.";
        public static string PriceLowerThanZero = "Price must me greater than 0";
    }
}
