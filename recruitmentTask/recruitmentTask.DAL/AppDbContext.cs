﻿using Microsoft.EntityFrameworkCore;
using RecruitmentTask.DAL.Entities;

namespace RecruitmentTask.DAL
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions options) : base(options)
        {
        }

        public AppDbContext()
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseInMemoryDatabase(databaseName: "Expert");
        }

        public DbSet<Product> Products { get; set; }
    }
}
