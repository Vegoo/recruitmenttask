﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using RecruitmentTask.DAL.Entities;
using RecruitmentTask.Services.Command;
using RecruitmentTask.Services.Query;

namespace RecruitmentTask.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProductController : Controller
    {
        private readonly IMediator _mediator;
        public ProductController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<ActionResult<List<Product>>> GetAllProducts()
        {
            var request = new GetAllProductsQuery();

            var result = await _mediator.Send(request);

            return Ok(result);
        }

        [HttpPost]
        public async Task<ActionResult> AddProduct([FromBody] AddProductCommand req)
        {
            await _mediator.Send(req);

            return Ok();
        }
    }
}
