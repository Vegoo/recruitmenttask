﻿using MediatR;
using RecruitmentTask.DAL.Entities;

namespace RecruitmentTask.Services.Query
{
    public class GetAllProductsQuery : IRequest<List<Product>>
    {

    }
}
