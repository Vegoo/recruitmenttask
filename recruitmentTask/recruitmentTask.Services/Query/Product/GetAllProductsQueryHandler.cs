﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using RecruitmentTask.DAL;
using RecruitmentTask.DAL.Entities;
using System;

namespace RecruitmentTask.Services.Query
{
    public class GetAllProductsQueryHandler : IRequestHandler<GetAllProductsQuery, List<Product>>
    {
        private readonly AppDbContext _appDbContext;

        public GetAllProductsQueryHandler(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }
        public async Task<List<Product>> Handle(GetAllProductsQuery request, CancellationToken cancellationToken)
        {
            return await _appDbContext.Products.ToListAsync();
        }
    }
}
