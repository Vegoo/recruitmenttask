﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecruitmentTask.Services.Command
{
    public class AddProductCommand : IRequest<Unit>
    {
        public string Barcode { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
    }
}
