﻿using MediatR;
using RecruitmentTask.DAL;
using RecruitmentTask.Services.Exceptions;
using RecruitmentTask.Commons;
using RecruitmentTask.DAL.Entities;

namespace RecruitmentTask.Services.Command
{
    public class AddProductCommandHandler : IRequestHandler<AddProductCommand, Unit>
    {
        private readonly AppDbContext _appDbContext;

        public AddProductCommandHandler(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }
        public async Task<Unit> Handle(AddProductCommand req, CancellationToken cancellationToken)
        {
            Product product = new Product()
            {
                Barcode = req.Barcode,
                Name = req.Name,
                Price = req.Price
            };

            if (_appDbContext.Products.Any(x => x.Barcode == product.Barcode))
            {
                throw new BadRequestException(ErrorMessages.BarcodeInUsing);
            }

            if (req.Price <= 0)
            {
                throw new BadRequestException(ErrorMessages.PriceLowerThanZero);
            }

            await _appDbContext.Products.AddAsync(product);
            await _appDbContext.SaveChangesAsync();

            return Unit.Value;
        }
    }
}
